# 2.0.0

- Update to Kotlin 1.7.20
- Update to Ktor 2.2.1
- Update to SLF4J 2.0.6

