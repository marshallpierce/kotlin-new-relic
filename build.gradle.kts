import java.net.URI
import java.time.Duration


plugins {
    kotlin("jvm") version "1.7.20" apply false
    id("org.jetbrains.dokka") version "1.7.20" apply false
    id("io.github.gradle-nexus.publish-plugin") version "1.1.0"
    id("net.researchgate.release") version "3.0.2"
    id("com.github.ben-manes.versions") version "0.44.0"
    id("org.jmailen.kotlinter") version "3.12.0" apply false
}

subprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "org.jmailen.kotlinter")

    repositories {
        mavenCentral()
    }

    @Suppress("UNUSED_VARIABLE")
    val deps by extra {
        mapOf(
            "ktor" to "2.2.1",
            "junit" to "5.9.1",
            "slf4j" to "2.0.6"
        )
    }

    tasks.named<Test>("test") {
        useJUnitPlatform()
    }

    configure<JavaPluginExtension> {
        toolchain {
            languageVersion.set(JavaLanguageVersion.of(11))
            vendor.set(JvmVendorSpec.AZUL)
        }
    }
}


subprojects.filter { listOf("ktor-new-relic", "coroutines-new-relic").contains(it.name) }.forEach { project ->
    project.run {
        apply(plugin = "maven-publish")
        apply(plugin = "signing")
        apply(plugin = "org.jetbrains.dokka")

        tasks {
            register<Jar>("docJar") {
                from(project.tasks["dokkaHtml"])
                archiveClassifier.set("javadoc")
            }
        }

        configure<JavaPluginExtension> {
            withSourcesJar()
        }

        configure<PublishingExtension> {
            publications {
                register<MavenPublication>("sonatype") {
                    from(components["java"])
                    artifact(tasks["docJar"])
                    // sonatype required pom elements
                    pom {
                        name.set("${project.group}:${project.name}")
                        description.set(name)
                        url.set("https://bitbucket.org/marshallpierce/kotlin-new-relic")
                        licenses {
                            license {
                                name.set("Copyfree Open Innovation License 0.5")
                                url.set("https://copyfree.org/content/standard/licenses/coil/license.txt")
                            }
                        }
                        developers {
                            developer {
                                id.set("marshallpierce")
                                name.set("Marshall Pierce")
                                email.set("575695+marshallpierce@users.noreply.github.com")
                            }
                        }
                        scm {
                            connection.set("scm:git:https://bitbucket.org/marshallpierce/kotlin-new-relic")
                            developerConnection.set("scm:git:ssh://git@bitbucket.org:marshallpierce/kotlin-new-relic.git")
                            url.set("https://bitbucket.org/marshallpierce/kotlin-new-relic")
                        }
                    }
                }
            }

            // A safe throw-away place to publish to:
            // ./gradlew publishSonatypePublicationToLocalDebugRepository -Pversion=foo
            repositories {
                maven {
                    name = "localDebug"
                    url = URI.create("file:///${project.buildDir}/repos/localDebug")
                }
            }
        }

        // don't barf for devs without signing set up
        if (project.hasProperty("signing.keyId")) {
            configure<SigningExtension> {
                sign(project.extensions.getByType<PublishingExtension>().publications["sonatype"])
            }
        }

        // releasing should publish
        rootProject.tasks.afterReleaseBuild {
            dependsOn(provider { project.tasks.named("publishToSonatype") })
        }
    }
}

nexusPublishing {
    repositories {
        sonatype {
            // sonatypeUsername and sonatypePassword properties are used automatically
            stagingProfileId.set("ab8c5618978d18") // org.mpierce
        }
    }
    // these are not strictly required. The default timeouts are set to 1 minute. But Sonatype can be really slow.
    // If you get the error "java.net.SocketTimeoutException: timeout", these lines will help.
    connectTimeout.set(Duration.ofMinutes(3))
    clientTimeout.set(Duration.ofMinutes(3))
}

release {
    git {
        requireBranch.set("master")
    }
}
