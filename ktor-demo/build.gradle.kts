plugins {
    application
}

val deps: Map<String, String> by extra

dependencies {
    implementation(project(":ktor-new-relic"))
    implementation(project(":coroutines-new-relic"))
    implementation("io.ktor", "ktor-server-netty", deps["ktor"])

    runtimeOnly("ch.qos.logback", "logback-classic", "1.4.5")

    implementation("com.zaxxer", "HikariCP", "4.0.3")
    runtimeOnly("org.postgresql", "postgresql", "42.2.19")
}

application {
    mainClass.set("org.mpierce.ktor.newrelic.KtorDemoMain")
}
