val deps: Map<String, String> by extra

dependencies {
    api("com.newrelic.agent.java", "newrelic-api", "6.4.2")
    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.6.4")
    implementation("org.slf4j", "slf4j-api", deps["slf4j"])
}

group = "org.mpierce.kotlin.coroutines.newrelic"
